<?php
// 订单模块
namespace JyKeruyun\Keruyun;

use JyKeruyun\Kernel\Http;

trait Order
{
  /**
   * 取订单列表
   * https://open.keruyun.com/docs/zh/QceVEXQBzPVmqdQuwVqZ.html
   *
   * @param array $params
   * @return bool
   */
  public function getOrderList($params = [])
  {
    $params = $this->handleTime($params);
    $url    = $this->createURL('/open/v1/data/order/export2');
    $res    = Http::httpPostJson($url, array_merge([
      'shopIdenty' => $this->shop_id,
    ], $params));
    return $this->handleReturn($res);
  }
  
  /**
   * 处理时间
   *
   * @param array $params
   * @return mixed
   */
  private function handleTime($params)
  {
    if (!isset($params['startTime'])) {
      $params['startTime'] = strtotime(date('Y-m-d 00:00:00', time())) * 1000;
      $params['endTime']   = strtotime(date('Y-m-d 23:59:59', time())) * 1000;
      
      // 昨天-现在，前天-现在...，最高查7天前到现在
    } else if ($params['startTime'] < 0 && $params['startTime'] > -8) {
      $params['startTime'] = strtotime(date('Y-m-d 00:00:00', strtotime($params['startTime'] . ' day', time()))) * 1000;
      $params['endTime']   = strtotime(date('Y-m-d 23:59:59', time())) * 1000;
      
    } else {
      $params['startTime'] = $params['startTime'] * 1000;
      $params['endTime']   = $params['endTime'] * 1000;
    }
    
    return $params;
  }
  
  /**
   * 取订单详情
   *
   * @param array $ids
   * @return bool
   */
  public function getOrderDetail($ids)
  {
    $url           = $this->createURL('/open/v1/data/order/exportDetail');
    $res           = Http::httpPostJson($url, [
      'ids'        => $ids,
      'shopIdenty' => $this->shop_id,
    ]);
    return $this->handleReturn($res);
  }
}
