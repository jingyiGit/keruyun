<?php

namespace JyKeruyun\Keruyun;

use JyKeruyun\Kernel\Http;
use JyKeruyun\Keruyun\Shop;
use JyKeruyun\Keruyun\Takeout;
use JyKeruyun\Keruyun\Dish;
use JyKeruyun\Keruyun\Order;
use JyKeruyun\Keruyun\Table;

class Keruyun
{
  use Shop;
  use Takeout;
  use Dish;
  use Order;
  use Table;
  
  protected $domainUrl = 'https://openapi.keruyun.com';
  protected $config = [];
  protected $cookies = null; // 当前开放帐号的Cookies
  protected $token = null;   // 店家授权到当前应用中的token
  protected $shop_id = null; // 店家在客如云里的商户编号
  protected $error = null;
  
  public function __construct($config = null)
  {
    $this->config = $config;
  }
  
  /**
   * 登录开放平台(未知有什么用)
   *
   * @return 失败返回false，成功返回相关信息
   */
  public function login($user, $pass, &$msg = '')
  {
    $res = Http::httpPost('https://open.keruyun.com/i/signIn', [
      'phoneNumber' => $user,
      'password'    => $pass,
      'areaCode'    => '86',
      'autoSignIn'  => 'true',
    ], [
      'Content-Type: application/json',
    ]);
    if ($res['code'] == 0) {
      $this->cookies = Http::getCookies();
      return $res['result'];
    }
    $msg = $res;
    return false;
  }
  
  /**
   * 获取服务下可用门店
   * https://open.keruyun.com/docs/zh/SceTEXQBzPVmqdQuzVhc.html
   *
   * @param string $appId 服务Id
   * @return false|mixed
   */
  public function getAppShops($appId)
  {
    $this->shop_id = '-1';
    $this->token   = null;
    $url           = $this->createURL('/open/v1/shop/fetchAppShops');
    $res           = Http::httpPostJson($url, [
      'appId' => $appId,
    ]);
    return $this->handleReturn($res);
  }
  
  /**
   * 创建签名 URL
   *
   * @param $url
   * @return string
   */
  public function createURL($url)
  {
    $params = [
      'appKey'     => $this->config['key'],
      'version'    => '1.0',
      'timestamp'  => time(),
      'shopIdenty' => $this->shop_id,
    ];
    if (!$this->shop_id) {
      $this->setError('shop_id不能为空');
      return false;
    }
    return $this->domainUrl . $url . '?' . http_build_query($params) . '&sign=' . $this->getSign($params);
  }
  
  public function getSign($data)
  {
    //步骤一：按字典排序参数
    $str = $this->formatBizQueryParaMap($data, false);
    //步骤二：在str后面加key/token
    if (is_null($this->token)) {
      $str .= $this->config['secret'];
    } else {
      $str .= $this->token;
    }
    
    //步骤三：sha256校验，并转为大写
    return hash('sha256', $str);
  }
  
  public function formatBizQueryParaMap($paraMap, $urlencode)
  {
    $buff = '';
    ksort($paraMap);
    
    foreach ($paraMap as $k => $v) {
      if ($urlencode) {
        $v = urlencode($v);
      }
      $buff .= $k . $v;
    }
    return $buff;
  }
  
  private function handleReturn($res)
  {
    if ($res['code'] == 0 && $res['apiMessage'] == null) {
      $this->error = null;
      return $res['result'];
    }
    $this->setError($res);
    return false;
  }
  
  public function getError()
  {
    return $this->error;
  }
  
  private function setError($error)
  {
    $this->error = $error;
    return false;
  }
}
